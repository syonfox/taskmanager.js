/**
 * Sample express router for task status
 */
var express = require('express');
var router = express.Router();

var utils = require('../utils');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});


let taskManager = require('taskmanager.js');;

router.get('/executeTask', async (req, res) => {
    res.json(taskManager.getStatus('whateverMyKeyIs'));
});

router.get('/taskManagerStatus', async (req, res) => {

    res.json(taskManager.getStatus());
});
router.get('/taskManagerTasks', async (req, res) => {

  // let d = taskManager.tasks.map(t)
  res.json(taskManager.tasks);
});
