# taskManager.js
[![npm version](https://badge.fury.io/js/taskmanager.js.svg)](https://badge.fury.io/js/taskmanager.js)


This is a simple npm module for task management in node.js using native spawn api and providing simple 
standard to display client side progress bars etc. You are still responsible for parsing output and spawning tasks

## Getting started

### Shell
```shell
wget https://gitlab.com/syonfox/taskmanager.js/-/raw/main/taskManager.js

git clone https://gitlab.com/syonfox/taskmanager.js.git
cp taskmanager.js/taskManager.js #path to your source

npm install taskmanager.js
```
### Node.js
```js
var taskManager = require('../path/to/taskmanager.js/taskManager.js') //direct file
var taskManager =  require('taskmanager.js') // npm 

 let task = taskManager.spawnTask('echo World', {
        parseProgress: (task, data, done, err) => {//task object, data = new stdout,  done, err are bools 
            if(done) {
                //task.stdout = compleat output
            }
            return parseInt(data); //todo actually figure out the progress made
            
        },
        args: ['42']
    },
 (tid, data, done) => {
        console.log('Hello ', data);
        if (done) {
            console.log('Woo Done');
            //todo start the next step or notify something
        }
    });

 app.get('/demoStatus', (req, res) => {
    taskManager.getStatus('')
})
```

### express router
```js
var express = require('express');
var router = express.Router();

var utils = require('../utils');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

let taskManager = require('taskmanager.js');;
router.get('/goproStatus', async (req, res) => {

    res.json(taskManager.getStatus('gopro'));
});
router.get('/taskManagerStatus', async (req, res) => {

    res.json(taskManager.getStatus());
});
router.get('/taskManagerTasks', async (req, res) => {

```

## Support
Just make an issue if you have problems suggestions


## Roadmap
Add full on graphical task manager lib fro clientside

Different scheduling algorithms for task throttling

## Contributing / Development


to make a npm package
```
npm init 
npm login
npm publish
```

to update it bump the npm version and publish again.

If you are developing locally and have the taslkmanager.js repo you can also link it to your project
```
cd path/to/taskmanager.js
sudo npm link

cd path/to/project
npm link taskmanager.js

#then include as if it was npm installed
const taskManager = require('taskmanager.js');
console.log(taskManager)
```


The advantage of doing it like so is you can make changes to the taskmanager.js project and they will immediately be reflected in your project without the need for pushing to npm and updating
note that sometimes this symlink gets destroyed when updating so you may have to relink at some point.

## Authors and acknowledgment
Thanks to [VGeo](vgeo.ca) and  [PLANmap](planmap.ca) for providing an environment where we can work towards 
the simple development of data systems.

## License
MIT

## Project status
Development / beta
